import numpy

from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB

import itertools
from statistics import mean

import matplotlib.pyplot as plt

from datetime import datetime
import os
from pathlib import Path
from dataclasses import dataclass

import sys


@dataclass
class Model:
    name: str
    optimParamNum : int
    optimParams : str = None
    
def getModels():
    models = []
    models.append(Model("SVM_Linear", 1, ["C"]))
    models.append(Model("Quadratic_Discriminant_Analysis", 1, ["Regularization parameter"]))
    models.append(Model("Linear_Discriminant_Analysis", 0))
    models.append(Model("Decision_Tree", 1, ["Max depth"]))
    models.append(Model("SVM_Polynomial", 2, ["C", "Gamma"]))
    models.append(Model("Logistic_Regression", 1, ["C"]))
    models.append(Model("KNeighbours", 1, ["Power parameter"]))
    models.append(Model("SVM_Rbf", 2, ["C", "Gamma"]))
    models.append(Model("Naive_Bayes", 1, ["Smoothing variable"]))
    
    return models

def getModel(name, models):
    for model in models:
        if model.name in name:
            return model
    print("Model not found!")
    sys.exit()
    

def testAccuracy(clf, data, y, testNum):
    rskf = RepeatedStratifiedKFold(n_splits=5, n_repeats=testNum)
    scl = StandardScaler()

    scores = []
#    i = 1
    for train, test in rskf.split(data, y):
        X_train = data[train,:]
        X_test = data[test,:]
        y_train = y[train]
        y_test = y[test]
        X_train = scl.fit_transform(X_train)
        X_test = scl.transform(X_test)
#        print(i)
#        i = i+1
        
        clf.fit(X_train, y_train)
        score = clf.score(X_test, y_test)
        scores.append(score)
    accuracy = mean(scores)
    return accuracy

def selectFeaturesBackward(data, y, datatype, folder, modelname):
    print("Feature selection started")
    
    clf = getDefaultClf(modelname)

    allFeatures = list(range(data.shape[1]))
    
    Accuracies = []
    TopFeatures = []
    featureNumToOne = numpy.arange(data.shape[1]-1,0,-1)
    for featureNum in featureNumToOne:
        
        featureCombinations = list(itertools.combinations(allFeatures, featureNum))        
        bestAccuracy = 0
        
        for features in featureCombinations:
            currdata = data[:, features]
            accuracy = testAccuracy(clf, currdata, y, 20)
            if accuracy > bestAccuracy:
                bestFeatures = features
                bestAccuracy = accuracy
        
        del allFeatures[allFeatures.index(list(set(allFeatures)-set(bestFeatures))[0])]     # Del worst feature
        Accuracies.append(bestAccuracy)
        TopFeatures.append(bestFeatures)

    BestFeatures = TopFeatures[Accuracies.index(max(Accuracies))]
    BestAccuracy = Accuracies[Accuracies.index(max(Accuracies))]
    Accuracies.insert(0, testAccuracy(clf, data, y, 20))   # Mind a 19
    Accuracies.reverse()
    return BestFeatures, BestAccuracy, Accuracies

def selectFeaturesForward(data, y, datatype, folder, modelname):
    print("Feature selection started")
    
    clf = getDefaultClf(modelname)
    
    allFeatures = list(range(data.shape[1]))
    
    Accuracies = []
    TopFeatures = []
    presentBestFeatures = []
    futureBestFeatures = []
    for featureNum in range(data.shape[1]):
        
        featureCombinations = list(itertools.combinations(allFeatures, 1))        
        bestAccuracy = 0
        presentBestFeatures = futureBestFeatures
        for feature in featureCombinations:
            currfeatures = presentBestFeatures.copy()
            currfeatures.append(feature[0])
            currdata = data[:, currfeatures]
            accuracy = testAccuracy(clf, currdata, y, 20)
            if accuracy > bestAccuracy:
                featureToDel = list(set(currfeatures)-set(presentBestFeatures))[0]
                futureBestFeatures = currfeatures
                bestAccuracy = accuracy
        
        del allFeatures[allFeatures.index(featureToDel)]
        Accuracies.append(bestAccuracy)
        TopFeatures.append(futureBestFeatures)
    
    BestFeatures = TopFeatures[Accuracies.index(max(Accuracies))]
    return BestFeatures, Accuracies[Accuracies.index(max(Accuracies))], Accuracies

def optimizeHyperparams(data, y, datatype, folder, model):
    print("Hyperparam opt started")
    
    if model.optimParamNum == 0:
        clf = getParamClf(model.name)
        accuracy = testAccuracy(clf, data, y, 200)
        return accuracy, 0, 0, clf
    
    if model.optimParamNum == 1:
        # Logarithmic scale
        params = getLogParams(model.name)
        Accuracy = []
        bestAccuracy = 0;
        j = 1
        for param in params:
#            print(j)
            j += 1
            clf = getParamClf(model.name, param)
            accuracy = testAccuracy(clf, data, y, 40)
            Accuracy.append(accuracy)
            if accuracy > bestAccuracy:
                bestParam = param
                bestAccuracy = accuracy
        
        plt.figure()
        plt.plot(params, Accuracy, 'ro')
        plt.xscale('log')
        plt.xlabel(model.optimParams[0])
        plt.ylabel("Accuracy")
        path = folder / datatype / "logHyperparams.png"
        plt.savefig(path)
        plt.close()
        
        i = 1
        while (max(Accuracy) - min(Accuracy)) > 0.005:
            step = bestParam*2**-i
            params = getParams(model.name, bestParam, step)    
            Accuracy = []
            bestAccuracy = 0;
            j = 1
            for param in params:
#                print(j)
                j += 1
                clf = getParamClf(model.name, param)
                accuracy = testAccuracy(clf, data, y, 40)
                Accuracy.append(accuracy)
                if accuracy > bestAccuracy:
                    bestParam = param
                    bestAccuracy = accuracy
    
            plt.figure()
            plt.plot(params, Accuracy, 'ro')
            plt.xlabel(model.optimParams[0])
            plt.ylabel("Accuracy")
            figurename = "Hyperparams" + str(i) + ".png"
            path = folder / datatype / figurename
            plt.savefig(path)
            plt.close()
            
            i += 1
            if i > 5:
                break
#            print(i)
            
        clf = getParamClf(model.name, bestParam)
        accuracy = testAccuracy(clf, data, y, 200)
        return accuracy, bestParam, 0, clf
    
    if model.optimParamNum == 2:
        # Logarithmic scales
        params1, params2 = getLogParams(model.name)
        Accuracy = []
        bestAccuracy = 0;
        j = 1
        for param in params1:
#            print(j)
            j += 1
            clf = getParam1Clf(model.name, param)
            accuracy = testAccuracy(clf, data, y, 40)
            Accuracy.append(accuracy)
            if accuracy > bestAccuracy:
                bestParam1 = param
                bestAccuracy = accuracy
        
        plt.figure()
        plt.plot(params1, Accuracy, 'ro')
        plt.xscale('log')
        plt.xlabel(model.optimParams[0])
        plt.ylabel("Accuracy")
        figurename = "logHyperparams" + model.optimParams[0] + ".png"
        path = folder / datatype / figurename
        plt.savefig(path)
        plt.close()
        
        
        Accuracy = []
        bestAccuracy = 0;
        j = 1
        for param in params2:
#            print(j)
            j += 1
            clf = getParam2Clf(model.name, param)
            accuracy = testAccuracy(clf, data, y, 40)
            Accuracy.append(accuracy)
            if accuracy > bestAccuracy:
                bestParam2 = param
                bestAccuracy = accuracy
        
        plt.figure()
        plt.plot(params2, Accuracy, 'ro')
        plt.xscale('log')
        plt.xlabel(model.optimParams[1])
        plt.ylabel("Accuracy")
        figurename = "logHyperparams" + model.optimParams[1] + ".png"
        path = folder / datatype / figurename
        plt.savefig(path)
        plt.close()
        
        #Grid - larger
        params1 = numpy.linspace(bestParam1/10, bestParam1*10, 25)
        params2 = numpy.linspace(bestParam2/10, bestParam2*10, 25)
        Accuracy = numpy.empty([params1.shape[0], params2.shape[0]])
        bestAccuracy = 0
        i = 0
        for param1 in params1:
            j = 0
            for param2 in params2:
                clf = getParamClf(model.name, param1, param2)
                accuracy = testAccuracy(clf, data, y, 20)
                Accuracy[i][j] = accuracy
                if accuracy > bestAccuracy:
                    bestParam1 = param1
                    bestParam2 = param2
                    bestAccuracy = accuracy
                j += 1
            i += 1
            
        plt.figure()
        plt.contourf(params2, params1, Accuracy, 20, cmap='RdGy')
        plt.xlabel(model.optimParams[1])
        plt.ylabel(model.optimParams[0])
        clb = plt.colorbar();
        clb.set_label('Accuracy')
        figurename = "HyperparamsColour0.png"
        path = folder / datatype / figurename
        plt.savefig(path)
        plt.close()
        
        # Grid - smaller
        h = 1
        while (numpy.amax(Accuracy) - numpy.amin(Accuracy)) > 0.005:
            stepParam1 = bestParam1*2**-h
            params1 = numpy.linspace(bestParam1-stepParam1, bestParam1+stepParam1, 10)
            stepParam2 = bestParam2*2**-h
            params2 = numpy.linspace(bestParam2-stepParam2, bestParam2+stepParam2, 10)
            Accuracy = numpy.empty([params1.shape[0], params2.shape[0]])
            bestAccuracy = 0
            i = 0
            for param1 in params1:
                j = 0
                for param2 in params2:
                    clf = getParamClf(model.name, param1, param2)
                    accuracy = testAccuracy(clf, data, y, 20)
                    Accuracy[i][j] = accuracy
                    if accuracy > bestAccuracy:
                        bestParam1 = param1
                        bestParam2 = param2
                        bestAccuracy = accuracy
                    j += 1
                i += 1
                

            plt.figure()
            plt.contourf(params2, params1, Accuracy, 20, cmap='RdGy')
            plt.xlabel(model.optimParams[1])
            plt.ylabel(model.optimParams[0])
            clb = plt.colorbar();
            clb.set_label('Accuracy')
            figurename = "HyperparamsColour" + str(h) + ".png"
            path = folder / datatype / figurename
            plt.savefig(path)
            plt.close()
            
#            print(h)
            h += 1
            if h > 5:
                break
    
    clf = getParamClf(model.name, bestParam1, bestParam2)
    accuracy = testAccuracy(clf, data, y, 200)
    return accuracy, bestParam1, bestParam2, clf

def getLabels():
    labelsdys = [1 for x in range(23)]
    labelsctrl = [2 for x in range(24)]
    y = labelsdys + labelsctrl
    y = numpy.array(y)
    return y

def getTimestamp():
    now = datetime.now()
    return datetime.timestamp(now)

def manageDirs(root, modelname, treshold):
    tresholdfolder = "P_Treshold_%.2f" % treshold
    foldername = modelname
    dirToCreate = root / "Results" / "Classification" / "Final" / tresholdfolder / foldername
        
    if not os.path.exists(dirToCreate):
        os.makedirs(dirToCreate)
    if not os.path.exists(dirToCreate / "Mean"):
        os.makedirs(dirToCreate / "Mean")
    if not os.path.exists(dirToCreate / "Median"):
        os.makedirs(dirToCreate / "Median")
    if not os.path.exists(dirToCreate / "Combinated"):
        os.makedirs(dirToCreate / "Combinated")
    
    now = datetime.now()
    dt_string = now.strftime("%y-%m-%d_%H-%M")
    
    filename = "log_ " + modelname + "_" + dt_string + ".txt"
        
    f = open(root / "Results" / "Classification" / "Final" / tresholdfolder / foldername / filename, "w+")
        
    return (f, dirToCreate)

def getLogParams(modelname):
    if modelname == "SVM_Linear":
        return numpy.logspace(-3,3)
    if modelname == "Quadratic_Discriminant_Analysis":
        return numpy.logspace(-6,0)
    if "SVM_Polynomial" in modelname or modelname == "SVM_Rbf":
        return numpy.logspace(-3,3), numpy.logspace(-5,1)
    if modelname == "Decision_Tree":
        return gen_log_space(1000,51)
    if modelname == "Logistic_Regression":
        return numpy.logspace(-3,3)
    if "KNeighbours" in modelname:
        return numpy.logspace(0,3)
    if modelname == "Naive_Bayes":
        return numpy.logspace(-12,3)
    
def getParams(modelname, bestParam, step):
    if modelname == "SVM_Linear":
        return numpy.linspace(bestParam-step, bestParam+step, 25)
    if modelname == "Quadratic_Discriminant_Analysis":
        if bestParam+step > 1:
            return numpy.linspace(bestParam-step*2, 1, 25)    
        else:
            return numpy.linspace(bestParam-step, bestParam+step, 25)
    if modelname == "Decision_Tree":
        if bestParam-step < 1:
            return numpy.linspace(bestParam-step+1, bestParam+step+1, 10, dtype=int)
        else:
            return numpy.linspace(bestParam-step, bestParam+step, 10, dtype=int)
    if modelname == "Logistic_Regression":
        return numpy.linspace(bestParam-step, bestParam+step, 25)
    if "KNeighbours" in modelname:
        if bestParam - step < 1:
            return numpy.linspace(1, bestParam+step*2, 25)    
        else:
            return numpy.linspace(bestParam-step, bestParam+step, 25)
    if modelname == "Naive_Bayes":
        return numpy.linspace(bestParam-step, bestParam+step, 25)

def getDefaultClf(modelname):
    if modelname == "SVM_Linear":
        return SVC(kernel='linear', C=1.0, gamma='auto')
    if modelname == "Quadratic_Discriminant_Analysis":
        return QuadraticDiscriminantAnalysis(reg_param=1.0)
    if modelname == "Linear_Discriminant_Analysis":
        return LinearDiscriminantAnalysis()
    if modelname == "Decision_Tree":
        return DecisionTreeClassifier()
    if "SVM_Polynomial" in modelname:
        degreelist = [int(s) for s in modelname if s.isdigit()]
        return SVC(kernel='poly', degree=degreelist[0], C=1.0, gamma='auto')
    if modelname == "Logistic_Regression":
        return LogisticRegression(C=1.0, solver='liblinear', multi_class='ovr')
    if "KNeighbours" in modelname:
        numnelist = [int(s) for s in modelname if s.isdigit()]
        return KNeighborsClassifier(n_neighbors=numnelist[0])
    if modelname == "SVM_Rbf":
        return SVC(kernel='rbf', C=1.0, gamma='auto')
    if modelname == "Naive_Bayes":
        return GaussianNB()

def getParamClf(modelname, param1=None, param2=None):
    if modelname == "SVM_Linear":
        return SVC(kernel='linear', C=param1, gamma='auto')
    if modelname == "Quadratic_Discriminant_Analysis":
        return QuadraticDiscriminantAnalysis(reg_param=param1)
    if modelname == "Linear_Discriminant_Analysis":
        return LinearDiscriminantAnalysis()
    if modelname == "Decision_Tree":
        return DecisionTreeClassifier(max_depth=param1)
    if "SVM_Polynomial" in modelname:
        degreelist = [int(s) for s in modelname if s.isdigit()]
        return SVC(kernel='poly', degree=degreelist[0], C=param1, gamma=param2)
    if modelname == "Logistic_Regression":
        return LogisticRegression(C=param1, solver='liblinear', multi_class='ovr')
    if "KNeighbours" in modelname:
        numnelist = [int(s) for s in modelname if s.isdigit()]
        return KNeighborsClassifier(n_neighbors=numnelist[0], p=param1)
    if modelname == "SVM_Rbf":
        return SVC(kernel='rbf', C=param1, gamma=param2)
    if modelname == "Naive_Bayes":
        return GaussianNB(var_smoothing=param1)
        
def getParam1Clf(modelname, param1):
    if "SVM_Polynomial" in modelname:
        degreelist = [int(s) for s in modelname if s.isdigit()]
        return SVC(kernel='poly', degree=degreelist[0], C=param1, gamma='auto')
    if modelname == "SVM_Rbf":
        return SVC(kernel='rbf', C=param1, gamma='auto')
    
def getParam2Clf(modelname, param2):
    if "SVM_Polynomial" in modelname:
        degreelist = [int(s) for s in modelname if s.isdigit()]
        return SVC(kernel='poly', degree=degreelist[0], C=1.0, gamma=param2)
    if modelname == "SVM_Rbf":
        return SVC(kernel='rbf', C=1.0, gamma=param2)
    
def writeParamMessage(f, modelname, accuracy, param1, param2):
    if modelname == "SVM_Linear":
        f.write("Best C: %.2f, Accuracy: %.2f%%\n\n" % (param1, accuracy*100))
    if modelname == "Quadratic_Discriminant_Analysis":
        f.write("Best Regularization parameter: %.2f, Accuracy: %.2f%%\n\n" % (param1, accuracy*100))
    if modelname == "Linear_Discriminant_Analysis":
        f.write("Accuracy: %.2f%%\n\n" % (accuracy*100))
    if modelname == "Decision_Tree":
        f.write("Best Max depth: %.2f, Accuracy: %.2f%%\n\n" % (param1, accuracy*100))
    if "SVM_Polynomial" in modelname or modelname == "SVM_Rbf":
        f.write("Best C: %.2f, Best Gamma: %.5f, Accuracy: %.2f%%\n\n" % (param1, param2, accuracy*100))
    if modelname == "Logistic_Regression":
        f.write("Best C: %.2f, Accuracy: %.2f%%\n\n" % (param1, accuracy*100))
    if "KNeighbours" in modelname:
        f.write("Best Power parameter: %.2f, Accuracy: %.2f%%\n\n" % (param1, accuracy*100))
    if modelname == "Naive_Bayes":
        f.write("Best Smoothing variable: %.2f, Accuracy: %.2f%%\n\n" % (param1, accuracy*100))
        
def plotFeatureSelection(BackMean, ForMean, BackMedian, ForMedian, featureNum, modelname, folder):
    x = numpy.arange(1, len(BackMean)+1, 1)
    fig, axs = plt.subplots(2, 2, figsize=(12,9))
    axs[0, 0].plot(x, BackMean, 'bo')
    axs[0, 0].set(ylabel='Accuracy')
    axs[0, 0].set_xticks(numpy.arange(1, len(BackMean)+1, 2))
    axs[0, 0].set_title('Mean - Backward', fontsize=14)
    
    axs[0, 1].plot(x, ForMean, 'bo')
    axs[0, 1].set(ylabel='Accuracy')
    axs[0, 1].set_xticks(numpy.arange(1, len(ForMean)+1, 2))
    axs[0, 1].set_title('Mean - Forward', fontsize=14)
    
    x = numpy.arange(1, len(BackMedian)+1, 1)
    axs[1, 0].plot(x, BackMedian, 'ro')
    axs[1, 0].set(xlabel = 'Number of features', ylabel='Accuracy')
    axs[1, 0].set_xticks(numpy.arange(1, len(BackMedian)+1, 2))
    axs[1, 0].set_title('Median - Backward', fontsize=14)
    
    axs[1, 1].plot(x, ForMedian, 'ro')
    axs[1, 1].set(xlabel = 'Number of features', ylabel='Accuracy')
    axs[1, 1].set_xticks(numpy.arange(1, len(ForMedian)+1, 2))
    axs[1, 1].set_title('Median - Forward', fontsize=14)
    fig.suptitle("Feature selection comparison - " + modelname, fontsize=20)
    
#    figurename = "Feature selection.png"
    plt.savefig(folder / "Feature selection.png")
        
def gen_log_space(limit, n):
    result = [1]
    if n>1:  # just a check to avoid ZeroDivisionError
        ratio = (float(limit)/result[-1]) ** (1.0/(n-len(result)))
    while len(result)<n:
        next_value = result[-1]*ratio
        if next_value - result[-1] >= 1:
            # safe zone. next_value will be a different integer
            result.append(next_value)
        else:
            # problem! same integer. we need to find next_value by artificially incrementing previous value
            result.append(result[-1]+1)
            # recalculate the ratio so that the remaining values will scale correctly
            ratio = (float(limit)/result[-1]) ** (1.0/(n-len(result)))
    # round, re-adjust to 0 indexing (i.e. minus 1) and return np.uint64 array
    result = list(map(lambda x: round(x)-1, result))
    result.remove(0)
    return numpy.array(result, dtype=numpy.uint64)
