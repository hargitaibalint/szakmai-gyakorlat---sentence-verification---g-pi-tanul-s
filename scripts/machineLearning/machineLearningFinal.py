import pandas as pd

from machineLearningFunctions import selectFeaturesForward
from machineLearningFunctions import selectFeaturesBackward
from machineLearningFunctions import optimizeHyperparams
from machineLearningFunctions import getLabels
from machineLearningFunctions import getTimestamp
from machineLearningFunctions import manageDirs
from machineLearningFunctions import writeParamMessage
from machineLearningFunctions import getModels
from machineLearningFunctions import getModel

from pathlib import Path

from time import strftime
from time import gmtime

from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler

import matplotlib.pyplot as plt

models = getModels()
modelnames = ["Linear_Discriminant_Analysis", "Quadratic_Discriminant_Analysis", "Decision_Tree",
               "SVM_Linear", "SVM_Polynomial_1",  "SVM_Polynomial_3",  "SVM_Polynomial_5", "SVM_Rbf",
               "Logistic_Regression", "KNeighbours_3", "KNeighbours_5", "KNeighbours_9", "Naive_Bayes"]

Treshold = 1.0
tresholdfolder = "P_Treshold_%.2f" % Treshold

root = "C:\\Users\\Balint\\Desktop\\MTA\\Data\\sentenceVerification"
root = Path(root)
    
y = getLabels()

filename = root / "Results" / "withOutliers" / "SelectedForML" / tresholdfolder / "DataMean.csv"
dataPD = pd.read_csv(filename)
names = list(dataPD.columns)
Features = [0] * len(names)
dataMean = dataPD.to_numpy()

i = 0
crossvalNum = 5
Accuracies = [[] for _ in range(crossvalNum)]
skf = StratifiedKFold(n_splits=crossvalNum)
for train_index, test_index in skf.split(dataMean, y):
    X_train, X_test = dataMean[train_index], dataMean[test_index]
    y_train, y_test = y[train_index], y[test_index]
    for selectedmodelname in modelnames:
        
        model = getModel(selectedmodelname, models)
        originalname = model.name
        model.name = selectedmodelname
        print(model.name)
        (f, folder) = manageDirs(root, model.name, Treshold)
       
        f.write("Data - Mean\n")
       
        start = getTimestamp()
    
        bestFeaturesBackward, accuracyBackward, BackwardAccuraciesMean = selectFeaturesBackward(X_train, y_train, "Mean", folder, model.name)
        end = getTimestamp()
        f.write("Backward feature selection duration: " + strftime("%H:%M:%S", gmtime(round(end-start,2))) + "\n")
        f.write("Best features: " + ''.join("%s, " % a for a in [names[i] for i in bestFeaturesBackward]) + "\n")
        for feature in bestFeaturesBackward:
            Features[feature] += 1
           
        start = getTimestamp()
        bestFeaturesForward, accuracyForward, ForwardAccuraciesMean = selectFeaturesForward(X_train, y_train, "Mean", folder, model.name)
        end = getTimestamp()
        f.write("Forward feature selection duration: " + strftime("%H:%M:%S", gmtime(round(end-start,2))) + "\n")
        f.write("Best features: " + ''.join("%s, " % a for a in [names[i] for i in bestFeaturesForward]) + "\n")
        for feature in bestFeaturesForward:
            Features[feature] += 1
    #      
        if accuracyBackward >= accuracyForward:
            bestFeatures = bestFeaturesBackward
            f.write("Better method: Backward\n")
        else:
            bestFeatures = bestFeaturesForward
            f.write("Better method: Forward\n")
       
        start = getTimestamp()
        X_trainSelected = X_train[:, bestFeatures]
        meanaccuracy, param1, param2, clf = optimizeHyperparams(X_trainSelected, y_train, "Mean", folder, model)
        end = getTimestamp()
        f.write("Hyperparameter optimization duration: " + strftime("%H:%M:%S", gmtime(round(end-start,2))) + "\n")
        writeParamMessage(f, model.name, meanaccuracy, param1, param2)
        
        scl = StandardScaler()   
        X_trainTransformed = scl.fit_transform(X_trainSelected)
        X_testTransformed = scl.transform(X_test[:, bestFeatures])
        clf.fit(X_trainTransformed, y_train)
        score = clf.score(X_testTransformed, y_test)
        Accuracies[i].append(score*100)
        f.write("Final accuracy: %.2f%%\n" % (score*100))
        f.close()
        model.name = originalname
        
    plt.figure(figsize=(10,8))
    x = list(range(1, len(modelnames)+1))
    plt.plot(x, Accuracies[i], 'ro')
    plt.xticks(x, modelnames, rotation='vertical')
    plt.ylabel("Accuracy [%]", fontsize=14)
    plt.title("Accuracy of different models", fontsize=16)
    plt.tight_layout()
    figurename = "Accuracies" + str(i) + ".png"
    figpath = root / "Results" / "Classification" / "Final" / tresholdfolder / figurename
    plt.savefig(figpath)
    plt.close()
  
    plt.figure(figsize=(20,8))
    x = list(range(1, len(names)+1))
    plt.plot(x, Features, 'bo')
    plt.xticks(x, names, rotation='vertical', fontsize=12)
    plt.ylabel("Prevalence", fontsize=14)
    plt.title("Prevalence of different features", fontsize=16)
    plt.tight_layout()
    figurename = "Features" + str(i) + ".png"
    figpath = root / "Results" / "Classification" / "Final" / tresholdfolder / figurename
    plt.savefig(figpath)
    plt.close()
    
    i += 1

FinalAccuracies = []
for i in range(len(modelnames)):
    accuracies = []
    for j in range(crossvalNum):
        accuracies.append(Accuracies[j][i])
    FinalAccuracies.append((sum(accuracies)/len(accuracies)))
plt.figure(figsize=(10,8))
x = list(range(1, len(modelnames)+1))
plt.plot(x, FinalAccuracies, 'ro')
plt.xticks(x, modelnames, rotation='vertical')
plt.ylabel("Accuracy [%]", fontsize=14)
plt.title("Accuracy of different models", fontsize=16)
plt.tight_layout()
figpath = root / "Results" / "Classification" / "Final" / tresholdfolder / "FinalAccuracies.png"
plt.savefig(figpath)
plt.close()