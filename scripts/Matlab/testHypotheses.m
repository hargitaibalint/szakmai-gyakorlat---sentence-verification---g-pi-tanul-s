function [ptMean, puMean, ptMedian, puMedian] = testHypotheses(root, var)

DysMean = getfield(load([root, var, 'DysMean.mat']), [var, 'DysMean']);
CtrlMean = getfield(load([root, var, 'CtrlMean.mat']), [var, 'CtrlMean']);

DysMedian = getfield(load([root, var, 'DysMedian.mat']), [var, 'DysMedian']);
CtrlMedian = getfield(load([root, var, 'CtrlMedian.mat']), [var, 'CtrlMedian']);

if contains(var, 'Ang')
    ptMean = circ_wwtest(transpose(DysMean), transpose(CtrlMean));
    puMean = circ_wwtest(transpose(DysMean), transpose(CtrlMean));
    
    ptMedian = circ_wwtest(transpose(DysMedian), transpose(CtrlMedian));
    puMedian = circ_wwtest(transpose(DysMedian), transpose(CtrlMedian));
else
    [~,ptMean,~] = ttest2(DysMean, CtrlMean);
    [puMean, ~] = ranksum(DysMean, CtrlMean);
    
    [~,ptMedian,~] = ttest2(DysMedian, CtrlMedian);
    [puMedian, ~] = ranksum(DysMedian, CtrlMedian);
end

end

