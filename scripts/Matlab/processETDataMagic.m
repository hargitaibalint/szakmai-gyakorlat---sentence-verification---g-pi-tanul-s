function processETDataMagic(subj, features, root)

global Results
names = subj(:,1);
conds = subj(:,2);
cDys = transpose(repelem("Dys", length(conds(cell2mat(conds(:))== 1))));
cCtrl = transpose(repelem("Ctrl", length(conds(cell2mat(conds(:))== 2))));
cond = [cDys; cCtrl];

for i = 1 : length(features)
    vars.(strcat(features(i), "Dys", "Mean")) = [];
    vars.(strcat(features(i), "Ctrl", "Mean")) = [];
    vars.(strcat(features(i), "Dys", "Median")) = [];
    vars.(strcat(features(i), "Ctrl", "Median")) = [];
    vars.(strcat(features(i), "Dys", "Diff")) = [];
    vars.(strcat(features(i), "Ctrl", "Diff")) = [];
end
% 
% for i = 1:length(subj)
%     if cell2mat(conds(i)) == 1
%         parseETtxt(string(fullfile('C:\Users\Balint\Desktop\MTA\Data\sentenceVerification\ET_data', char(names(i)), 'senver Samples.txt')), names(i), 'C:\Users\Balint\Desktop\MTA\Data');
%     else
%         parseETtxt(string(fullfile('C:\Users\Balint\Desktop\MTA\Data\sentenceVerification\ET_data', char(names(i)), [char(names(i)), '_senver Samples.txt'])), subj(i,1), 'C:\Users\Balint\Desktop\MTA\Data');
%     end
%     disp([names(i), ' ', num2str(i), ' done']);
% end

for i = 1:length(subj)

%     Alloc empty arrays
    for j = 1 : length(features)
        vars.(features(j)) = [];
    end
    
%     Get/calculate attributes
    load([root, '\ET_data\', char(names(i)), '\DetAll.mat'], 'ETparams');
    for j = 1 : 44
        if (isfield(ETparams, 'saccadeInfo'))
            if (isfield(ETparams.saccadeInfo, 'amplitude'))
                for k = 1 : length([ETparams.saccadeInfo(1,j,:).amplitude])
                    if ~isnan(ETparams.saccadeInfo(1,j,k).amplitude)
                        if ETparams.saccadeInfo(1,j,k).amplitude > 0
                            vars.SaccAmp = [vars.SaccAmp, ETparams.saccadeInfo(1,j,k).amplitude];
                            vars.SaccDir = [vars.SaccDir, ETparams.saccadeInfo(1,j,k).forward];
                            vars.SaccAng = [vars.SaccAng, ETparams.saccadeInfo(1,j,k).angle*pi/180];
                            vars.SaccVel = [vars.SaccVel, ETparams.saccadeInfo(1,j,k).peakVelocity];
                            vars.SaccAcc = [vars.SaccAcc, ETparams.saccadeInfo(1,j,k).peakAcceleration];
                            vars.SaccDur = [vars.SaccDur, ETparams.saccadeInfo(1,j,k).duration];
                            if (ETparams.saccadeInfo(1,j,k).forward == 1)
                                vars.FSaccAmp = [vars.FSaccAmp, ETparams.saccadeInfo(1,j,k).amplitude];
                                vars.FSaccVel = [vars.FSaccVel, ETparams.saccadeInfo(1,j,k).peakVelocity];
                                vars.FSaccAcc = [vars.FSaccAcc, ETparams.saccadeInfo(1,j,k).peakAcceleration];
                                vars.FSaccDur = [vars.FSaccDur, ETparams.saccadeInfo(1,j,k).duration];
                            else
                                vars.BSaccAmp = [vars.BSaccAmp, ETparams.saccadeInfo(1,j,k).amplitude];
                                vars.BSaccVel = [vars.BSaccVel, ETparams.saccadeInfo(1,j,k).peakVelocity];
                                vars.BSaccAcc = [vars.BSaccAcc, ETparams.saccadeInfo(1,j,k).peakAcceleration];
                                vars.BSaccDur = [vars.BSaccDur, ETparams.saccadeInfo(1,j,k).duration];
                            end
                        end
                    end
                end
                if ~isnan(ETparams.saccadeInfo(1,j,1).amplitude)
                    if ETparams.saccadeInfo(1,j,1).amplitude > 0
                        % MaxSpan, TotSweep
                        saccadeIdx = bwlabel(ETparams.saccadeIdx(j).Idx);
                        saccade_starts = zeros(1,max(saccadeIdx));
                        saccade_ends = zeros(1,max(saccadeIdx));
                        if ~isempty(saccade_ends)
                            for saci = 1:numel(saccade_ends)
                                saccade_starts(saci) = find(saccadeIdx==saci, 1, 'first');
                                saccade_ends(saci) = find(saccadeIdx==saci, 1, 'last');
                            end
                        end
                        if length(saccade_starts) > 1
                            maxspan = pixel2degree(range(ETparams.data(j).X(saccade_ends)));
                            vars.MaxSpan = [vars.MaxSpan, maxspan];
                            vars.TotSweep = [vars.TotSweep, pixel2degree(nansum(abs(diff((ETparams.data(j).X(saccade_ends))))))];
                            fsaccnum = nnz([ETparams.saccadeInfo(1,j,:).forward]);
                            bsaccnum = length([ETparams.saccadeInfo(1,j,:).forward]) - fsaccnum;
                            vars.FSaccNumPerMaxSpan = [vars.FSaccNumPerMaxSpan, fsaccnum / maxspan];
                            vars.BSaccNumPerMaxSpan = [vars.BSaccNumPerMaxSpan, bsaccnum / maxspan];
                        end
                    end
                end
            end
        end
        
        if (isfield(ETparams, 'glissadeInfo'))
            if (isfield(ETparams.glissadeInfo, 'amplitude'))
                for k = 1 : length([ETparams.saccadeInfo(1,j,:).amplitude])
                    if ~(isnan(ETparams.glissadeInfo(1,j,k).amplitude))
                        if ETparams.glissadeInfo(1,j,k).amplitude > 0
                            vars.GlissAmp = [vars.GlissAmp, ETparams.glissadeInfo(1,j,k).amplitude];
                            vars.GlissDir = [vars.GlissDir, ETparams.glissadeInfo(1,j,k).forward];
                            vars.GlissAng = [vars.GlissAng, ETparams.glissadeInfo(1,j,k).angle*pi/180];
                            vars.GlissVel = [vars.GlissVel, ETparams.glissadeInfo(1,j,k).peakVelocity];
                            vars.GlissDur = [vars.GlissDur, ETparams.glissadeInfo(1,j,k).duration];
                            if (ETparams.glissadeInfo(1,j,k).forward == 1)
                                vars.FGlissAmp = [vars.FGlissAmp, ETparams.glissadeInfo(1,j,k).amplitude];
                                vars.FGlissVel = [vars.FGlissVel, ETparams.glissadeInfo(1,j,k).peakVelocity];
                                vars.FGlissDur = [vars.FGlissDur, ETparams.glissadeInfo(1,j,k).duration];
                            else
                                vars.BGlissAmp = [vars.BGlissAmp, ETparams.glissadeInfo(1,j,k).amplitude];
                                vars.BGlissVel = [vars.BGlissVel, ETparams.glissadeInfo(1,j,k).peakVelocity];
                                vars.BGlissDur = [vars.BGlissDur, ETparams.glissadeInfo(1,j,k).duration];
                            end
                            if (ETparams.glissadeInfo(1,j,k).type == 2)
                                vars.HGlissAmp = [vars.HGlissAmp, ETparams.glissadeInfo(1,j,k).amplitude];
                                vars.HGlissVel = [vars.HGlissVel, ETparams.glissadeInfo(1,j,k).peakVelocity];
                                vars.HGlissDur = [vars.HGlissDur, ETparams.glissadeInfo(1,j,k).duration];
                            elseif (ETparams.glissadeInfo(1,j,k).type == 1)
                                vars.LGlissAmp = [vars.LGlissAmp, ETparams.glissadeInfo(1,j,k).amplitude];
                                vars.LGlissVel = [vars.LGlissVel, ETparams.glissadeInfo(1,j,k).peakVelocity];
                                vars.LGlissDur = [vars.LGlissDur, ETparams.glissadeInfo(1,j,k).duration];
                            end
                        end
                    end
                end
                glissade_det = single(cat(1,ETparams(1).glissadeInfo(1,j,:).numberOf));
                if ~isempty(glissade_det)
                    vars.GlissWC = [vars.GlissWC, sum(glissade_det(:,1))];
                    vars.GlissSC = [vars.GlissSC, sum(glissade_det(:,2))];
                end
                
            end
        end
        
        for k = 1 : length([ETparams.fixationInfo(1,1,:).duration])
            if ~(isnan(ETparams.fixationInfo(1,j,k).duration))
                if ETparams.fixationInfo(1,j,k).duration > 0
                    vars.FixDur = [vars.FixDur, ETparams.fixationInfo(1,j,k).duration];
                    vars.FixXPos = [vars.FixXPos, ETparams.fixationInfo(1,j,k).X];
                    vars.FixYPos = [vars.FixYPos, ETparams.fixationInfo(1,j,k).Y];
                end
            end
        end
        vars.FixNum = length([ETparams.fixationInfo.duration]);
    end
    vars.SaccNum = length(vars.SaccAmp);
    vars.FSaccNum = length(vars.SaccDir(vars.SaccDir == 1));
    vars.BSaccNum = length(vars.SaccDir(vars.SaccDir == 0));

    vars.GlissNum = length(vars.GlissAmp);
    vars.FGlissNum = length(vars.GlissDir(vars.GlissDir == 1));
    vars.BGlissNum = length(vars.GlissDir(vars.GlissDir == 0));

    vars.SaccGliss = 100*vars.GlissNum/vars.SaccNum;
    
    vars.AvgFixNoise = [ETparams.data.avgNoise];
    vars.StdFixNoise = [ETparams.data.stdNoise];
    
    % Calculate means
    for j = 1 : length(features)
        if contains(features(j), 'Gliss')
            if ~isempty(vars.GlissAmp)
                if contains(features(j), 'Ang')
                    avg(j) = circ_mean(transpose(vars.GlissAng))*180/pi;
                else
                    avg(j) = nanmean(vars.(features(j)));
                end
            end
        else
            if contains(features(j), 'Ang')
                avg(j) = circ_mean(transpose(vars.SaccAng))*180/pi;
            else
                avg(j) = nanmean(vars.(features(j)));
            end
        end
    end
    
    % Calculate medians
    for j = 1 : length(features)
       if contains(features(j), 'Gliss')
           if ~isempty(vars.GlissAmp)
               if contains(features(j), 'Ang')
                   median(j) = circ_median(transpose(vars.GlissAng))*180/pi;
               elseif contains(features(j), 'Dir')
                   median(j) = mean(vars.GlissDir);
               else
                   median(j) = nanmedian(vars.(features(j)));
               end
           end
       else
           if contains(features(j), 'Ang')
               median(j) = circ_median(transpose(vars.SaccAng))*180/pi;
           elseif contains(features(j), 'Dir')
               median(j) = mean(vars.SaccDir);
           else
               median(j) = nanmedian(vars.(features(j)));
           end
       end
    end
    
    % Write averages
    for j = 1 : length(features)
        vars.(strcat(features(j), cond(i), "Mean")) = [vars.(strcat(features(j), cond(i), "Mean")), avg(j)];
        vars.(strcat(features(j), cond(i), "Median")) = [vars.(strcat(features(j), cond(i), "Median")), median(j)];
        vars.(strcat(features(j), cond(i), "Diff")) = [vars.(strcat(features(j), cond(i), "Diff")), (avg(j) - median(j)) / median(j)];
    end
end

for i = 1 : length(features)
    if contains(features(i), 'Ang')
        Results.(features(i)).DysMean = circ_mean(transpose(vars.(strcat(features(i), "Dys", "Mean"))));
        Results.(features(i)).CtrlMean = circ_mean(transpose(vars.(strcat(features(i), "Ctrl", "Mean"))));
        Results.(features(i)).DysMedian = circ_median(transpose(vars.(strcat(features(i), "Dys", "Median"))));
        Results.(features(i)).CtrlMedian = circ_median(transpose(vars.(strcat(features(i), "Ctrl", "Median"))));
    else
        Results.(features(i)).DysMean = nanmean(vars.(strcat(features(i), "Dys", "Mean")));
        Results.(features(i)).CtrlMean = nanmean(vars.(strcat(features(i), "Ctrl", "Mean")));
        Results.(features(i)).DysMedian = nanmedian(vars.(strcat(features(i), "Dys", "Median")));
        Results.(features(i)).CtrlMedian = nanmedian(vars.(strcat(features(i), "Ctrl", "Median")));
    end
    Results.(features(i)).DysStdMean = nanstd(vars.(strcat(features(i), "Dys", "Mean")));
    Results.(features(i)).CtrlStdMean = std(vars.(strcat(features(i), "Ctrl", "Mean")));
    Results.(features(i)).DysStdMedian = nanstd(vars.(strcat(features(i), "Dys", "Median")));
    Results.(features(i)).CtrlStdMedian = std(vars.(strcat(features(i), "Ctrl", "Median")));
    
    Results.(features(i)).DysDiff = nanmean(vars.(strcat(features(i), "Dys", "Diff")));
    Results.(features(i)).CtrlDiff = nanmean(vars.(strcat(features(i), "Ctrl", "Diff")));

    % Save results, replace nans with means/medians
    varNameDys = [convertStringsToChars(features(i)), 'Dys', 'Mean'];
    varNameCtrl = [convertStringsToChars(features(i)), 'Ctrl', 'Mean'];
    clear SDys;
    clear SCtrl;
    SDys.(varNameDys) = vars.(strcat(features(i), "Dys", "Mean"));
    if nnz(isnan(SDys.(varNameDys))) > 0
        [~, col] = find(isnan(SDys.(varNameDys)));
        SDys.(varNameDys)(col) = nanmean(SDys.(varNameDys));
    end
    SCtrl.(varNameCtrl) = vars.(strcat(features(i), "Ctrl", "Mean"));
    if nnz(isnan(SCtrl.(varNameCtrl))) > 0
        [~, col] = find(isnan(SCtrl.(varNameCtrl)));
        SCtrl.(varNameCtrl)(col) = nanmean(SCtrl.(varNameCtrl));
    end
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameDys), '.mat'], '-struct', 'SDys');
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameCtrl), '.mat'], '-struct', 'SCtrl');
    
    varNameDys = [convertStringsToChars(features(i)), 'Dys', 'Median'];
    varNameCtrl = [convertStringsToChars(features(i)), 'Ctrl', 'Median'];
    clear SDys;
    clear SCtrl;
    SDys.(varNameDys) = vars.(strcat(features(i), "Dys", "Median"));
    if nnz(isnan(SDys.(varNameDys))) > 0
        [~, col] = find(isnan(SDys.(varNameDys)));
        SDys.(varNameDys)(col) = nanmedian(SDys.(varNameDys));
    end
    SCtrl.(varNameCtrl) = vars.(strcat(features(i), "Ctrl", "Median"));
    if nnz(isnan(SCtrl.(varNameCtrl))) > 0
        [~, col] = find(isnan(SCtrl.(varNameCtrl)));
        SCtrl.(varNameCtrl)(col) = nanmedian(SCtrl.(varNameCtrl));
    end
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameDys), '.mat'], '-struct', 'SDys');
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameCtrl), '.mat'], '-struct', 'SCtrl');
    
    % Remove outliers
% %     [~, OD] = rmoutliers(vars.(strcat(features(i), "Dys")));
% %     [~, OC] = rmoutliers(vars.(strcat(features(i), "Ctrl")));
% %     if contains(features(i), 'Ang')
% %         vars.(strcat(features(i), "Dys"))(OD) = circ_median(vars.(strcat(features(i), "Dys")));
% %         vars.(strcat(features(i), "Ctrl"))(OC) = circ_median(vars.(strcat(features(i), "Ctrl")));
% %     else
% %         vars.(strcat(features(i), "Dys"))(OD) = median(vars.(strcat(features(i), "Dys")));
% %         vars.(strcat(features(i), "Ctrl"))(OC) = median(vars.(strcat(features(i), "Ctrl")));
% %     end
%     
%     
%     if contains(features(i), 'Ang')
%         ODs = ODs + transpose(OD);
%         OCs = OCs + transpose(OC);
%     else
%         ODs = ODs + OD;
%         OCs = OCs + OC;
%     end
%     
%     
%     if contains(features(i), 'Ang')
%         Results.(features(i)).DysmedianO = circ_median(vars.(strcat(features(i), "Dys")));
%         Results.(features(i)).CtrlmedianO = circ_median(vars.(strcat(features(i), "Ctrl")));
%     else
%         Results.(features(i)).DysmedianO = nanmedian(vars.(strcat(features(i), "Dys")));
%         Results.(features(i)).CtrlmedianO = nanmedian(vars.(strcat(features(i), "Ctrl")));
%     end
%     Results.(features(i)).DysStdO = nanstd(vars.(strcat(features(i), "Dys")));
%     Results.(features(i)).CtrlStdO = std(vars.(strcat(features(i), "Ctrl")));
    
    
%     varNameDys = [convertStringsToChars(features(i)), 'Dys'];
%     varNameCtrl = [convertStringsToChars(features(i)), 'Ctrl'];
%     clear SDys;
%     clear SCtrl;
%     SDys.(varNameDys) = vars.(strcat(features(i), "Dys"));
%     SCtrl.(varNameCtrl) = vars.(strcat(features(i), "Ctrl"));
%     save([root, '\Results\withoutOutliers\', convertStringsToChars(varNameDys), '.mat'], '-struct', 'SDys');
%     save([root, '\Results\withoutOutliers\', convertStringsToChars(varNameCtrl), '.mat'], '-struct', 'SCtrl');
end

% figure(1);
% histogram(vars.SaccAmpDys, 'Binwidth', range(vars.SaccAmpCtrl)/6);
% hold on
% histogram(vars.SaccAmpCtrl, 'Binwidth', range(vars.SaccAmpCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Average saccade amplitude [°]');
% ylabel('Number of subjects');
% title('Sentence verification - Saccade amplitude comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccAmpHist.png']);
% 
% figure(2);
% boxplot([vars.SaccAmpDys vars.SaccAmpCtrl], [ones(size(vars.SaccAmpDys)), 2*ones(size(vars.SaccAmpCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Average saccade amplitude [°]');
% title('Sentence verification - Saccade amplitude comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccAmpBox.png']);
% 
% figure(3);
% histogram(vars.SaccDirDys, 'Binwidth', range(vars.SaccDirCtrl)/6);
% hold on
% histogram(vars.SaccDirCtrl, 'Binwidth', range(vars.SaccDirCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Saccade forwardness rate');
% ylabel('Number of subjects');
% title('Sentence verification - Saccade direction comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccDirHist.png']);
% 
% figure(4);
% boxplot([vars.SaccDirDys vars.SaccDirCtrl], [ones(size(vars.SaccDirDys)), 2*ones(size(vars.SaccDirCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Saccade forwardness rate');
% title('Sentence verification - Saccade direction comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccDirBox.png']);
% 
% figure(5);
% histogram(vars.SaccAngDys, 'Binwidth', range(vars.SaccAngCtrl)/6);
% hold on
% histogram(vars.SaccAngCtrl, 'Binwidth', range(vars.SaccAngCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Average saccade angle [°]');
% ylabel('Number of subjects');
% title('Sentence verification - Saccade angle comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccAngHist.png']);
% 
% figure(6);
% boxplot([transpose(vars.SaccAngDys) transpose(vars.SaccAngCtrl)], [ones(size(transpose(vars.SaccAngDys))), 2*ones(size(transpose(vars.SaccAngCtrl)))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Saccade angle [°]');
% title('Sentence verification - Saccade angle comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccAngBox.png']);
% 
% figure(7);
% histogram(vars.SaccVelDys, 'Binwidth', range(vars.SaccVelCtrl)/6);
% hold on
% histogram(vars.SaccVelCtrl, 'Binwidth', range(vars.SaccVelCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Saccade velocity [°/s]');
% ylabel('Number of subjects');
% title('Sentence verification - Saccade velocity comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccVelHist.png']);
% 
% figure(8);
% boxplot([vars.SaccVelDys vars.SaccVelCtrl], [ones(size(vars.SaccVelDys)), 2*ones(size(vars.SaccVelCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Saccade velocity [°/s]');
% title('Sentence verification - Saccade velocity comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccVelBox.png']);
% 
% figure(9);
% histogram(vars.SaccAccDys, 'Binwidth', range(vars.SaccAccCtrl)/6);
% hold on
% histogram(vars.SaccAccCtrl, 'Binwidth', range(vars.SaccAccCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Saccade acceleration [°/s^2]');
% ylabel('Number of subjects');
% title('Sentence verification - Saccade acceleration comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccAccHist.png']);
% 
% figure(10);
% boxplot([vars.SaccAccDys vars.SaccAccCtrl], [ones(size(vars.SaccAccDys)), 2*ones(size(vars.SaccAccCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Saccade acceleration [°/s¡2]');
% title('Sentence verification - Saccade acceleration comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccAccBox.png']);
% 
% figure(11);
% histogram(vars.SaccDurDys, 'Binwidth', range(vars.SaccDurCtrl)/6);
% hold on
% histogram(vars.SaccDurCtrl, 'Binwidth', range(vars.SaccDurCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Saccade duration [s]');
% ylabel('Number of subjects');
% title('Sentence verification - Saccade duration comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccDurHist.png']);
% 
% figure(12);
% boxplot([vars.SaccDurDys vars.SaccDurCtrl], [ones(size(vars.SaccDurDys)), 2*ones(size(vars.SaccDurCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Saccade duration [s]');
% title('Sentence verification - Saccade duration comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccDurBox.png']);
% 
% figure(13);
% histogram(vars.GlissAmpDys, 'Binwidth', range(vars.GlissAmpCtrl)/6);
% hold on
% histogram(vars.GlissAmpCtrl, 'Binwidth', range(vars.GlissAmpCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Average glissade amplitude [°]');
% ylabel('Number of subjects');
% title('Sentence verification - Glissade amplitude comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissAmpHist.png']);
% % 
% figure(14);
% boxplot([vars.GlissAmpDys vars.GlissAmpCtrl], [ones(size(vars.GlissAmpDys)), 2*ones(size(vars.GlissAmpCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Average glissade amplitude [°]');
% title('Sentence verification - Glissade amplitude comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissAmpBox.png']);
% % 
% figure(15);
% histogram(vars.GlissDirDys, 'Binwidth', range(vars.GlissDirCtrl)/6);
% hold on
% histogram(vars.GlissDirCtrl, 'Binwidth', range(vars.GlissDirCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Glissade forwardness rate');
% ylabel('Number of subjects');
% title('Sentence verification - Glissade direction comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissDirHist.png']);
% 
% figure(16);
% boxplot([vars.GlissDirDys vars.GlissDirCtrl], [ones(size(vars.GlissDirDys)), 2*ones(size(vars.GlissDirCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Glissade forwardness rate');
% title('Sentence verification - Glissade direction comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissDirBox.png']);
% 
% figure(17);
% histogram(vars.GlissAngDys, 'Binwidth', range(vars.GlissAngCtrl)/6);
% hold on
% histogram(vars.GlissAngCtrl, 'Binwidth', range(vars.GlissAngCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Average glissade angle [°]');
% ylabel('Number of subjects');
% title('Sentence verification - Glissade angle comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissAngHist.png']);

% figure(18);
% boxplot([transpose(vars.GlissAngDys) transpose(vars.GlissAngCtrl)], [ones(size(transpose(vars.GlissAngDys))), 2*ones(size(transpose(vars.GlissAngCtrl)))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Glissade angle [°]');
% title('Sentence verification - Glissade angle comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissAngBox.png']);
% 
% figure(19);
% histogram(vars.GlissVelDys, 'Binwidth', range(vars.GlissVelCtrl)/8);
% hold on
% histogram(vars.GlissVelCtrl, 'Binwidth', range(vars.GlissVelCtrl)/8);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Glissade velocity [°/s]');
% ylabel('Number of subjects');
% title('Sentence verification - Glissade velocity comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissVelHist.png']);
% 
% figure(20);
% boxplot([vars.GlissVelDys vars.GlissVelCtrl], [ones(size(vars.GlissVelDys)), 2*ones(size(vars.GlissVelCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Glissade velocity [°/s]');
% title('Sentence verification - Glissade velocity comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissVelBox.png']);
% 
% figure(21);
% histogram(vars.GlissDurDys, 'Binwidth', range(vars.GlissDurCtrl)/6);
% hold on
% histogram(vars.GlissDurCtrl, 'Binwidth', range(vars.GlissDurCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Glissade duration [s]');
% ylabel('Number of subjects');
% title('Sentence verification - Glissade duration comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissDurHist.png']);
% 
% figure(22);
% boxplot([vars.GlissDurDys vars.GlissDurCtrl], [ones(size(vars.GlissDurDys)), 2*ones(size(vars.GlissDurCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Glissade duration [s]');
% title('Sentence verification - Glissade duration comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'GlissDurBox.png']);
% 
% figure(23);
% histogram(vars.FixDurDys, 'Binwidth', range(vars.FixDurCtrl)/6);
% hold on
% histogram(vars.FixDurCtrl, 'Binwidth', range(vars.FixDurCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Average fixation time [s]');
% ylabel('Number of subjects');
% title('Sentence verification - Fixation time comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'FixDurHist.png']);
% 
% figure(24);
% boxplot([vars.FixDurDys vars.FixDurCtrl], [ones(size(vars.FixDurDys)), 2*ones(size(vars.FixDurCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Average fixation time [s]');
% title('Sentence verification - Fixation time comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'FixDurBox.png']);
% 
% figure(25);
% histogram(vars.SaccGlissDys, 'Binwidth', range(vars.SaccGlissCtrl)/6);
% hold on
% histogram(vars.SaccGlissCtrl, 'Binwidth', range(vars.SaccGlissCtrl)/6);
% legend('Dys', 'Ctrl', 'Location', 'best');
% xlabel('Percentage of saccades followed by glissades');
% ylabel('Number of subjects');
% title('Sentence verification - Percentage of saccades followed by glissades');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccGlissHist.png']);
% 
% figure(26);
% boxplot([vars.SaccGlissDys vars.SaccGlissCtrl], [ones(size(vars.SaccGlissDys)), 2*ones(size(vars.SaccGlissCtrl))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Percentage of saccades followed by glissades');
% title('Sentence verification - Percentage of saccades followed by glissades');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SaccGlissBox.png']);
% 
% for i = 14 : length(features)
%     figure(i+13);
%     boxplot([vars.(strcat(features(i), "Dys")) vars.(strcat(features(i), "Ctrl"))], ...
%         [ones(size(vars.(strcat(features(i), "Dys")))), 2*ones(size(vars.(strcat(features(i), "Ctrl"))))]);
%     xlabel('Type  -  1 = Dys, 2 = Ctrl');
%     ylabel(features(i));
%     title(features(i));
%     saveas(gcf, strcat(root, "\Results\withOutliers\figures\", features(i), ".png"));
% end

end