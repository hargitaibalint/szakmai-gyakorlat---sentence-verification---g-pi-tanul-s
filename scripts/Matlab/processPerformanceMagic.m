function processPerformanceMagic(subj, features, root)

global Results
names = subj(:,1);
conds = subj(:,2);
cDys = transpose(repelem("Dys", length(conds(cell2mat(conds(:))== 1))));
cCtrl = transpose(repelem("Ctrl", length(conds(cell2mat(conds(:))== 2))));
cond = [cDys; cCtrl];

for i = 1 : length(features)
   vars.(strcat(features(i), "Dys", "Mean")) = [];
   vars.(strcat(features(i), "Ctrl", "Mean")) = [];
   vars.(strcat(features(i), "Dys", "Median")) = [];
   vars.(strcat(features(i), "Ctrl", "Median")) = [];
   vars.(strcat(features(i), "Dys", "Diff")) = [];
   vars.(strcat(features(i), "Ctrl", "Diff")) = [];
end

load(fullfile(root, 'scripts', 'mondatverifikacio.mat'));
for i = 1:length(subj)
    load(fullfile(root, 'Behav_data', names{i}, 'SubData.mat'));
    load(fullfile(root, 'Behav_data', names{i}, 'textPresentationParams.mat'));
    for j = 1:44
        if subjectAnswers(j) == 1 && mondatverifikacio{textOrder(j),3} == 'I' ...
            || subjectAnswers(j) == 0 && mondatverifikacio{textOrder(j),3} == 'H'
           answers(j,1) = 1;
        else
           answers(j,1) = 0;
        end
        speedword(j) = (count(mondatverifikacio(textOrder(j),4), ' ') + 1) / (stim.lineEnd(j) - stim.lineStart(j)); % Szavak száma / hossz
        speedchar(j) = strlength(mondatverifikacio(textOrder(j),4)) / (stim.lineEnd(j) - stim.lineStart(j)); % Karakterek száma / hossz
    end
    vars.(strcat(features(1), cond(i), "Mean")) = [vars.(strcat(features(1), cond(i), "Mean")), mean(answers)*100];
    vars.(strcat(features(2), cond(i), "Mean")) = [vars.(strcat(features(2), cond(i), "Mean")), mean(stim.lineEnd - stim.lineStart)];
    vars.(strcat(features(3), cond(i), "Mean")) = [vars.(strcat(features(3), cond(i), "Mean")), mean(speedword)];
    vars.(strcat(features(4), cond(i), "Mean")) = [vars.(strcat(features(4), cond(i), "Mean")), mean(speedchar)];
    
    vars.(strcat(features(1), cond(i), "Median")) = [vars.(strcat(features(1), cond(i), "Median")), mean(answers)*100];
    vars.(strcat(features(2), cond(i), "Median")) = [vars.(strcat(features(2), cond(i), "Median")), median(stim.lineEnd - stim.lineStart)];
    vars.(strcat(features(3), cond(i), "Median")) = [vars.(strcat(features(3), cond(i), "Median")), median(speedword)];
    vars.(strcat(features(4), cond(i), "Median")) = [vars.(strcat(features(4), cond(i), "Median")), median(speedchar)];
    
    vars.(strcat(features(1), cond(i), "Diff")) = [vars.(strcat(features(1), cond(i), "Diff")), 0];
    vars.(strcat(features(2), cond(i), "Diff")) = [vars.(strcat(features(2), cond(i), "Diff")), mean(stim.lineEnd - stim.lineStart) - median(stim.lineEnd - stim.lineStart) / median(stim.lineEnd - stim.lineStart)];
    vars.(strcat(features(3), cond(i), "Diff")) = [vars.(strcat(features(3), cond(i), "Diff")), (mean(speedword) - median(speedword)) / median(speedword)];
    vars.(strcat(features(4), cond(i), "Diff")) = [vars.(strcat(features(4), cond(i), "Diff")), (mean(speedchar) - median(speedchar)) / median(speedchar)];
    
end

for i = 1 : length(features)
    Results.(features(i)).DysMean = nanmean(vars.(strcat(features(i), "Dys", "Mean")));
    Results.(features(i)).CtrlMean = nanmean(vars.(strcat(features(i), "Ctrl", "Mean")));
    Results.(features(i)).DysMedian = nanmedian(vars.(strcat(features(i), "Dys", "Median")));
    Results.(features(i)).CtrlMedian = nanmedian(vars.(strcat(features(i), "Ctrl", "Median")));
    
    Results.(features(i)).DysStdMean = nanstd(vars.(strcat(features(i), "Dys", "Mean")));
    Results.(features(i)).CtrlStdMean = std(vars.(strcat(features(i), "Ctrl", "Mean")));
    Results.(features(i)).DysStdMedian = nanstd(vars.(strcat(features(i), "Dys", "Median")));
    Results.(features(i)).CtrlStdMedian = std(vars.(strcat(features(i), "Ctrl", "Median")));
    
    Results.(features(i)).DysDiff = nanmean(vars.(strcat(features(i), "Dys", "Diff")));
    Results.(features(i)).CtrlDiff = nanmean(vars.(strcat(features(i), "Ctrl", "Diff")));
    
    varNameDys = [convertStringsToChars(features(i)), 'Dys', 'Mean'];
    varNameCtrl = [convertStringsToChars(features(i)), 'Ctrl', 'Mean'];
    clear SDys;
    clear SCtrl;
    SDys.(varNameDys) = vars.(strcat(features(i), "Dys", "Mean"));
    SCtrl.(varNameCtrl) = vars.(strcat(features(i), "Ctrl", "Mean"));
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameDys), '.mat'], '-struct', 'SDys');
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameCtrl), '.mat'], '-struct', 'SCtrl');
    
    varNameDys = [convertStringsToChars(features(i)), 'Dys', 'Median'];
    varNameCtrl = [convertStringsToChars(features(i)), 'Ctrl', 'Median'];
    clear SDys;
    clear SCtrl;
    SDys.(varNameDys) = vars.(strcat(features(i), "Dys", "Median"));
    SCtrl.(varNameCtrl) = vars.(strcat(features(i), "Ctrl", "Median"));
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameDys), '.mat'], '-struct', 'SDys');
    save([root, '\Results\withOutliers\', convertStringsToChars(varNameCtrl), '.mat'], '-struct', 'SCtrl');
    
%     Remove outliers
%     [~, OD] = rmoutliers(vars.(strcat(features(i), "Dys")));
%     [~, OC] = rmoutliers(vars.(strcat(features(i), "Ctrl")));
%     
%     vars.(strcat(features(i), "Dys"))(OD) = median(vars.(strcat(features(i), "Dys")));
%     vars.(strcat(features(i), "Ctrl"))(OC) = median(vars.(strcat(features(i), "Ctrl")));
%     
%     Results.(features(i)).DysmedianO = median(vars.(strcat(features(i), "Dys")));
%     Results.(features(i)).CtrlmedianO = median(vars.(strcat(features(i), "Ctrl")));
%     Results.(features(i)).DysStdO = std(vars.(strcat(features(i), "Dys")));
%     Results.(features(i)).CtrlStdO = std(vars.(strcat(features(i), "Ctrl")));
%     
%     varNameDys = [convertStringsToChars(features(i)), 'Dys'];
%     varNameCtrl = [convertStringsToChars(features(i)), 'Ctrl'];
%     clear SDys;
%     clear SCtrl;
%     SDys.(varNameDys) = vars.(strcat(features(i), "Dys"));
%     SCtrl.(varNameCtrl) = vars.(strcat(features(i), "Ctrl"));
%     save([root, '\Results\withoutOutliers\', convertStringsToChars(varNameDys), '.mat'], '-struct', 'SDys');
%     save([root, '\Results\withoutOutliers\', convertStringsToChars(varNameCtrl), '.mat'], '-struct', 'SCtrl');
end

% figure(1);
% histogram(vars.('ScoreDys'), 'Binwidth', range(vars.('ScoreCtrl')/4));
% hold on
% histogram(vars.('ScoreCtrl'), 'Binwidth', range(vars.('ScoreCtrl')/4));
% legend("Dys", "Ctrl", 'Location', 'best');
% xlabel('Average score [%]');
% ylabel('Number of subjects');
% title('Sentence verification - Score comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'ScoreHist.png']);
% 
% figure(2);
% histogram(vars.('TimeDys'), 'Binwidth', 0.5);
% hold on
% histogram(vars.('TimeCtrl'), 'Binwidth', 0.5);
% legend("Dys", "Ctrl", 'Location', 'best');
% xlabel('Average time [s]');
% ylabel('Number of subjects');
% title('Sentence verification - Time comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'TimeHist.png']);
% 
% figure(3);
% histogram(vars.('SpeedWordDys'), 'Binwidth', 0.5);
% hold on
% histogram(vars.('SpeedWordCtrl'), 'Binwidth', 0.5);
% legend("Dys", "Ctrl", 'Location', 'best');
% xlabel('Average reading speed [word/s]');
% ylabel('Number of subjects');
% title('Sentence verification - Reading speed comparison (words)');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SpeedWordHist.png']);
% 
% figure(4);
% histogram(vars.('SpeedCharDys'), 'Binwidth', 2.5);
% hold on
% histogram(vars.('SpeedCharCtrl'), 'Binwidth', 2.5);
% legend("Dys", "Ctrl", 'Location', 'best');
% xlabel('Average reading speed [characters/s]');
% ylabel('Number of subjects');
% title('Sentence verification - Reading speed comparison (characters)');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SpeedCharHist.png']);
% 
% figure(5);
% boxplot([vars.('ScoreDys') vars.('ScoreCtrl')], [ones(size(vars.('ScoreDys'))), 2*ones(size(vars.('ScoreCtrl')))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Average score [%]');
% title('Sentence verification - Score comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'ScoreBox.png']);
% 
% figure(6);
% boxplot([vars.('TimeDys') vars.('TimeCtrl')], [ones(size(vars.('TimeDys'))), 2*ones(size(vars.('TimeCtrl')))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Average time [s]');
% title('Sentence verification - Time comparison');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'TimeBox.png']);
% 
% figure(7);
% boxplot([vars.('SpeedWordDys') vars.('SpeedWordCtrl')], [ones(size(vars.('SpeedWordDys'))), 2*ones(size(vars.('SpeedWordCtrl')))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Average reading speed [word/s]');
% title('Sentence verification - Reading speed comparison (words)');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SpeedWordBox.png']);
% 
% figure(8);
% boxplot([vars.('SpeedCharDys') vars.('SpeedCharCtrl')], [ones(size(vars.('SpeedCharDys'))), 2*ones(size(vars.('SpeedCharCtrl')))]);
% xlabel('Type  -  1 = Dys, 2 = Ctrl');
% ylabel('Average reading speed [characters/s]');
% title('Sentence verification - Reading speed comparison (characters)');
% saveas(gcf, [root, '\Results\withOutliers\figures\', 'SpeedCharBox.png']);


end
