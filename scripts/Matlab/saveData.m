function saveData(root, Treshold)
% root = 'C:\Users\Balint\Desktop\MTA\Data\sentenceVerification';
load([root, '\Results\Results.mat']);

fields = fieldnames(Results);
dsMean = dataset();
dsMedian = dataset();
dsComb = dataset();

for i = 1:length(fields)
    if Results.(string(fields(i))).puMean < Treshold
        dysMean = load(strcat(root, '\Results\withOutliers\', string(fields(i)), 'DysMean.mat'));
        ctrlMean = load(strcat(root, '\Results\withOutliers\', string(fields(i)), 'CtrlMean.mat'));
        
        dsMean.(char(fields(i))) = [transpose(dysMean.(strcat(string(fields(i)), 'DysMean'))); ...
            transpose(ctrlMean.(strcat(string(fields(i)), 'CtrlMean')))];
    end
    
    if Results.(string(fields(i))).puMedian < Treshold
        dysMedian = load(strcat(root, '\Results\withOutliers\', string(fields(i)), 'DysMedian.mat'));
        ctrlMedian = load(strcat(root, '\Results\withOutliers\', string(fields(i)), 'CtrlMedian.mat'));
        
        dsMedian.(char(fields(i))) = [transpose(dysMedian.(strcat(string(fields(i)), 'DysMedian'))); ...
            transpose(ctrlMedian.(strcat(string(fields(i)), 'CtrlMedian')))];
    end
    
    if Results.(string(fields(i))).puMean < Treshold || Results.(string(fields(i))).puMedian < Treshold
        if Results.(string(fields(i))).puMean < Results.(string(fields(i))).puMedian
            dsComb.(char(fields(i))) = [transpose(dysMean.(strcat(string(fields(i)), 'DysMean'))); ...
                transpose(ctrlMean.(strcat(string(fields(i)), 'CtrlMean')))];
        else
            dsComb.(char(fields(i))) = [transpose(dysMedian.(strcat(string(fields(i)), 'DysMedian'))); ...
                transpose(ctrlMedian.(strcat(string(fields(i)), 'CtrlMedian')))];
        end
    end
end
foldername = [root, '\Results\withOutliers\SelectedForML\P_Treshold_', char(num2str(Treshold,'%.2f'))];
mkdir(foldername);

export(dsMean, 'File', [foldername, '\dataMean.csv'],'Delimiter',',');
export(dsMedian, 'File', [foldername, '\dataMedian.csv'],'Delimiter',',');
export(dsComb, 'File', [foldername, '\dataComb.csv'],'Delimiter',',');


% load([root, '\Results\withOutliers\FixDurCtrlMean.mat'])
% load([root, '\Results\withOutliers\FixDurDysMean.mat'])
% load([root, '\Results\withOutliers\FixNumCtrlMean.mat'])
% load([root, '\Results\withOutliers\FixNumDysMean.mat'])
% load([root, '\Results\withOutliers\FSaccNumCtrlMean.mat'])
% load([root, '\Results\withOutliers\FSaccNumDysMean.mat'])
% load([root, '\Results\withOutliers\GlissAmpDysMean.mat'])
% load([root, '\Results\withOutliers\GlissAmpCtrlMean.mat'])
% load([root, '\Results\withOutliers\GlissDurCtrlMean.mat'])
% load([root, '\Results\withOutliers\GlissDurDysMean.mat'])
% load([root, '\Results\withOutliers\GlissSCCtrlMean.mat'])
% load([root, '\Results\withOutliers\GlissSCDysMean.mat'])
% load([root, '\Results\withOutliers\GlissVelCtrlMean.mat'])
% load([root, '\Results\withOutliers\GlissVelDysMean.mat'])
% load([root, '\Results\withOutliers\GlissWCCtrlMean.mat'])
% load([root, '\Results\withOutliers\GlissWCDysMean.mat'])
% load([root, '\Results\withOutliers\HGlissVelCtrlMean.mat'])
% load([root, '\Results\withOutliers\HGlissVelDysMean.mat'])
% load([root, '\Results\withOutliers\MaxSpanCtrlMean.mat'])
% load([root, '\Results\withOutliers\MaxSpanDysMean.mat'])
% load([root, '\Results\withOutliers\SaccAccCtrlMean.mat'])
% load([root, '\Results\withOutliers\SaccAccDysMean.mat'])
% load([root, '\Results\withOutliers\SaccAmpCtrlMean.mat'])
% load([root, '\Results\withOutliers\SaccAmpDysMean.mat'])
% load([root, '\Results\withOutliers\SaccDurCtrlMean.mat'])
% load([root, '\Results\withOutliers\SaccDurDysMean.mat'])
% load([root, '\Results\withOutliers\SaccVelCtrlMean.mat'])
% load([root, '\Results\withOutliers\SaccVelDysMean.mat'])
% load([root, '\Results\withOutliers\ScoreCtrlMean.mat'])
% load([root, '\Results\withOutliers\ScoreDysMean.mat'])
% load([root, '\Results\withOutliers\SpeedCharCtrlMean.mat'])
% load([root, '\Results\withOutliers\SpeedCharDysMean.mat'])
% load([root, '\Results\withOutliers\SpeedWordCtrlMean.mat'])
% load([root, '\Results\withOutliers\SpeedWordDysMean.mat'])
% load([root, '\Results\withOutliers\TimeCtrlMean.mat'])
% load([root, '\Results\withOutliers\TimeDysMean.mat'])
% load([root, '\Results\withOutliers\TotSweepCtrlMean.mat'])
% load([root, '\Results\withOutliers\TotSweepDysMean.mat'])
% 
% load([root, '\Results\withOutliers\FixDurCtrlMedian.mat'])
% load([root, '\Results\withOutliers\FixDurDysMedian.mat'])
% load([root, '\Results\withOutliers\FixNumCtrlMedian.mat'])
% load([root, '\Results\withOutliers\FixNumDysMedian.mat'])
% load([root, '\Results\withOutliers\FSaccNumCtrlMedian.mat'])
% load([root, '\Results\withOutliers\FSaccNumDysMedian.mat'])
% load([root, '\Results\withOutliers\GlissAmpDysMedian.mat'])
% load([root, '\Results\withOutliers\GlissAmpCtrlMedian.mat'])
% load([root, '\Results\withOutliers\GlissDurCtrlMedian.mat'])
% load([root, '\Results\withOutliers\GlissDurDysMedian.mat'])
% load([root, '\Results\withOutliers\GlissSCCtrlMedian.mat'])
% load([root, '\Results\withOutliers\GlissSCDysMedian.mat'])
% load([root, '\Results\withOutliers\GlissVelCtrlMedian.mat'])
% load([root, '\Results\withOutliers\GlissVelDysMedian.mat'])
% load([root, '\Results\withOutliers\GlissWCCtrlMedian.mat'])
% load([root, '\Results\withOutliers\GlissWCDysMedian.mat'])
% load([root, '\Results\withOutliers\HGlissVelCtrlMedian.mat'])
% load([root, '\Results\withOutliers\HGlissVelDysMedian.mat'])
% load([root, '\Results\withOutliers\MaxSpanCtrlMedian.mat'])
% load([root, '\Results\withOutliers\MaxSpanDysMedian.mat'])
% load([root, '\Results\withOutliers\SaccAccCtrlMedian.mat'])
% load([root, '\Results\withOutliers\SaccAccDysMedian.mat'])
% load([root, '\Results\withOutliers\SaccAmpCtrlMedian.mat'])
% load([root, '\Results\withOutliers\SaccAmpDysMedian.mat'])
% load([root, '\Results\withOutliers\SaccDurCtrlMedian.mat'])
% load([root, '\Results\withOutliers\SaccDurDysMedian.mat'])
% load([root, '\Results\withOutliers\SaccVelCtrlMedian.mat'])
% load([root, '\Results\withOutliers\SaccVelDysMedian.mat'])
% load([root, '\Results\withOutliers\ScoreCtrlMedian.mat'])
% load([root, '\Results\withOutliers\ScoreDysMedian.mat'])
% load([root, '\Results\withOutliers\SpeedCharCtrlMedian.mat'])
% load([root, '\Results\withOutliers\SpeedCharDysMedian.mat'])
% load([root, '\Results\withOutliers\SpeedWordCtrlMedian.mat'])
% load([root, '\Results\withOutliers\SpeedWordDysMedian.mat'])
% load([root, '\Results\withOutliers\TimeCtrlMedian.mat'])
% load([root, '\Results\withOutliers\TimeDysMedian.mat'])
% load([root, '\Results\withOutliers\TotSweepCtrlMedian.mat'])
% load([root, '\Results\withOutliers\TotSweepDysMedian.mat'])
% 
% DysMean = transpose([TimeDysMean; ScoreDysMean; SpeedCharDysMean; SpeedWordDysMean; SaccAmpDysMean; SaccAccDysMean; SaccDurDysMean; SaccVelDysMean; ...
%     GlissAmpDysMean; GlissDurDysMean; GlissVelDysMean; FixDurDysMean; TotSweepDysMean; GlissSCDysMean; GlissWCDysMean; HGlissVelDysMean; MaxSpanDysMean; FixNumDysMean; FSaccNumDysMean]);
% 
% CtrlMean = transpose([TimeCtrlMean; ScoreCtrlMean; SpeedCharCtrlMean; SpeedWordCtrlMean; SaccAmpCtrlMean; SaccAccCtrlMean; SaccDurCtrlMean; SaccVelCtrlMean; ...
%     GlissAmpCtrlMean; GlissDurCtrlMean; GlissVelCtrlMean; FixDurCtrlMean; TotSweepCtrlMean; GlissSCCtrlMean; GlissWCCtrlMean; HGlissVelCtrlMean; MaxSpanCtrlMean; FixNumCtrlMean; FSaccNumCtrlMean]);
% 
% DataMean = [DysMean; CtrlMean];
% csvwrite([root, '\Results\DataMean.csv'], DataMean)
% 
% 
% DysMedian = transpose([TimeDysMedian; ScoreDysMedian; SpeedCharDysMedian; SpeedWordDysMedian; SaccAmpDysMedian; SaccAccDysMedian; SaccDurDysMedian; SaccVelDysMedian; ...
%     GlissAmpDysMedian; GlissDurDysMedian; GlissVelDysMedian; FixDurDysMedian; TotSweepDysMedian; GlissSCDysMedian; GlissWCDysMedian; HGlissVelDysMedian; MaxSpanDysMedian; FixNumDysMedian; FSaccNumDysMedian]);
% 
% CtrlMedian = transpose([TimeCtrlMedian; ScoreCtrlMedian; SpeedCharCtrlMedian; SpeedWordCtrlMedian; SaccAmpCtrlMedian; SaccAccCtrlMedian; SaccDurCtrlMedian; SaccVelCtrlMedian; ...
%     GlissAmpCtrlMedian; GlissDurCtrlMedian; GlissVelCtrlMedian; FixDurCtrlMedian; TotSweepCtrlMedian; GlissSCCtrlMedian; GlissWCCtrlMedian; HGlissVelCtrlMedian; MaxSpanCtrlMedian; FixNumCtrlMedian; FSaccNumCtrlMedian]);
% 
% DataMedian = [DysMedian; CtrlMedian];
% csvwrite([root, '\Results\DataMedian.csv'], DataMedian)
% 
% end

end

