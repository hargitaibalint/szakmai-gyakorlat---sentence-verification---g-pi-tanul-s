function subj = getSubjects

subj = {
    
% Dyslexic subjects

'buzs', 1;... % 1.
'csiki', 1;... % 2. // CSKI
'foti', 1;... % 3. NaN-ok
'luro', 1;... % 4. NaN-ok %%% he is actually vaba
'mead', 1;... % 5.
'move', 1;... % 6.
'pebe', 1;... % 7.
'vano', 1;... % 8.
'elki', 1;... % 9.
% discarded 'kada', 1;... % 10.
'kodo', 1;... % 11.
'komo', 1;... % 12.
'mava', 1;... % 13.
'sako', 1;... % 14.
% 'lurov', 1;...% 15. // MISSING
'rupa', 1;... % 16. NaN-ok
'opcs', 1;... % 17.
'boda', 1;... % 18.
'duda', 1;... % 19.
'foja', 1;... % 20.
'kani', 1;... % 21. %----- eddig a pontig meg az elso tipusu trigger_cleaning kell az EEG-nel
'hlvi', 1;... % 22.
'dobe', 1;... % 23.
'road', 1;... % 24.
'hoor', 1;... % 25.

% Control subjects

'bama', 2;... % 1.
% no pair 'bobe', 2;... % 2.
'beni', 2;... % 3.
'hoag', 2;... % 4.
'homa', 2;... % 5.
'koev', 2;... % 6.
'mofr', 2;... % 7.
'nemi', 2;... % 8.
'plda', 2;... % 9.
'foju', 2;... % 10.
'neda', 2;... % 11.
'buka', 2;... % 12.
'vikr', 2;... % 13.
% discarded 'siol', 2;... % 14. 
'tita', 2;... % 15.
'keli', 2;... % 16.
'szida', 2;... % 17.
'csda', 2;... % 18.
'doan', 2;... % 19.
'adre', 2;... % 20.
'fope', 2;... % 21.
% no pair 'haab', 2;... % 22.
'jaga', 2;... % 23.
'kocev', 2;... % 24.
'kori', 2;... % 25.
'tado', 2;... % 26.
'urma', 2;... % 27.
};

end