function testHypothesesMagic(features, root)
global Results
[Results.Time.ptMean, Results.Time.puMean, Results.Time.ptMedian, Results.Time.puMedian] = testHypotheses([root, '\Results\withOutliers\'], 'Time');
[Results.Score.ptMean, Results.Score.puMean, Results.Score.ptMedian, Results.Score.puMedian] = testHypotheses([root, '\Results\withOutliers\'], 'Score');
[Results.SpeedChar.ptMean, Results.SpeedChar.puMean, Results.SpeedChar.ptMedian, Results.SpeedChar.puMedian] = testHypotheses([root, '\Results\withOutliers\'], 'SpeedChar');
[Results.SpeedWord.ptMean, Results.SpeedWord.puMean, Results.SpeedWord.ptMedian, Results.SpeedWord.puMedian] = testHypotheses([root, '\Results\withOutliers\'], 'SpeedWord');

% [Results.Time.ptO, Results.Time.puO] = testHypotheses([root, '\Results\withoutOutliers\', 'Time');
% [Results.Score.ptO, Results.Score.puO] = testHypotheses([root, '\Results\withoutOutliers\', 'Score');
% [Results.SpeedChar.ptO, Results.SpeedChar.puO] = testHypotheses([root, '\Results\withoutOutliers\', 'SpeedChar');
% [Results.SpeedWord.ptO, Results.SpeedWord.puO] = testHypotheses([root, '\Results\withoutOutliers\', 'SpeedWord');

for i = 1 : length(features)
   [Results.(features(i)).ptMean, Results.(features(i)).puMean, Results.(features(i)).ptMedian, Results.(features(i)).puMedian] ...
       = testHypotheses([root, '\Results\withOutliers\'], convertStringsToChars(features(i)));
   
%    [Results.(features(i)).ptO, Results.(features(i)).puO] ...
%        = testHypotheses([root, '\Results\withoutOutliers\', convertStringsToChars(features(i)));
end

end